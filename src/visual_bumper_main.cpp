#include "ros/ros.h"
#include "tf/transform_listener.h"
#include "sensor_msgs/PointCloud.h"
#include "tf/message_filter.h"
#include "message_filters/subscriber.h"
#include "laser_geometry/laser_geometry.h"

#include "std_msgs/Bool.h"
#include <math.h> 

class LaserScanToPointCloud{

public:

  ros::NodeHandle n_;
  laser_geometry::LaserProjection projector_;
  tf::TransformListener listener_;
  message_filters::Subscriber<sensor_msgs::LaserScan> laser_sub_;
  tf::MessageFilter<sensor_msgs::LaserScan> laser_notifier_;
  ros::Publisher scan_pub_;
  ros::Publisher bumper_pub_;

  unsigned int cloud_size_;

  double bumper_front_limit_, bumper_back_limit_, 
        bumper_left_limit_, bumper_right_limit_,
        bumper_corner_remove_;

  LaserScanToPointCloud(ros::NodeHandle n) : 
    n_(n),
    // laser_sub_(n_, "base_scan", 10),
    laser_sub_(n_, "scan", 10),
    laser_notifier_(laser_sub_,listener_, "base_link", 10)
  {
    laser_notifier_.registerCallback(
      boost::bind(&LaserScanToPointCloud::scanCallback, this, _1));
    laser_notifier_.setTolerance(ros::Duration(0.01));
    scan_pub_ = n_.advertise<sensor_msgs::PointCloud>("/cloud_scan",1);
    bumper_pub_ = n_.advertise<std_msgs::Bool>("/visual_bumper_triggered",1);

    // Get visal bumper params
    n_.param("bumper_front_limit", bumper_front_limit_, 0.45);
    n_.param("bumper_back_limit", bumper_back_limit_, 0.45);
    n_.param("bumper_left_limit", bumper_left_limit_, 0.45);
    n_.param("bumper_right_limit", bumper_right_limit_, 0.45);

    n_.param("bumper_corner_remove", bumper_corner_remove_, 0.55);

    n_.getParam("bumper_front_limit", bumper_front_limit_);
    n_.getParam("bumper_back_limit", bumper_back_limit_);
    n_.getParam("bumper_left_limit", bumper_left_limit_);
    n_.getParam("bumper_right_limit", bumper_right_limit_);

    n_.getParam("bumper_corner_remove", bumper_corner_remove_);

    ROS_INFO("getParam bumper_front_limit = %4.2f.",(float)bumper_front_limit_);
    ROS_INFO("getParam bumper_back_limit = %4.2f.",(float)bumper_back_limit_);
    ROS_INFO("getParam bumper_left_limit = %4.2f.",(float)bumper_left_limit_);
    ROS_INFO("getParam bumper_right_limit = %4.2f.",(float)bumper_right_limit_);

    ROS_INFO("getParam bumper_corner_remove = %4.2f.",(float)bumper_corner_remove_);
    
  }

  void scanCallback (const sensor_msgs::LaserScan::ConstPtr& scan_in)
  {
    sensor_msgs::PointCloud cloud;
    std_msgs::Bool bumper_state;

    try
    {
        projector_.transformLaserScanToPointCloud(
          "base_link",*scan_in, cloud,listener_);
    }
    catch (tf::TransformException& e)
    {
        std::cout << e.what();
        return;
    }
    
    // Do something with cloud.
    cloud_size_ = cloud.points.size();
    ROS_INFO("cloud_size_ = %d.", cloud_size_);

    // for (unsigned int i=0; i<cloud_size_; i+=50) {
    //   float x = cloud.points[i].x;
    //   float y = cloud.points[i].y;
    //   float z = cloud.points[i].z;
    //   ROS_INFO("point[%d].xyz = [%4.2f,\t %4.2f, \t %4.2f].", i, x, y, z);
    // }    

    // print all channels.
    // ROS_INFO("cloud chanel size = %d", (unsigned int)cloud.channels.size());

    // Add another channel
    unsigned int chan_size = cloud.channels.size();
    cloud.channels.resize (chan_size + 2);
    cloud.channels[chan_size].name = "3D_distance";
    cloud.channels[chan_size].values.resize (cloud_size_);
    cloud.channels[chan_size+1].name = "visual_bumper";
    cloud.channels[chan_size+1].values.resize (cloud_size_);


    // Check resize result
    // ROS_INFO("cloud chanel size = %d,%d, after resize", 
    //   (unsigned int)cloud.channels.size(), 
    //   (unsigned int) cloud.channels[chan_size].values.size());

    bumper_state.data = false; // move this out of points loop.
    // Loop through all points
    for (unsigned int i=0; i<cloud_size_; i++) {
      float x = cloud.points[i].x;
      float y = cloud.points[i].y;
      float z = cloud.points[i].z;

      float dis_3d = cbrt(pow(x,2) + pow(y,2) + pow(z,2));
      //ROS_INFO("point[%d].xyz = [%4.2f,\t %4.2f, \t %4.2f].", i, x, y, z);

      // 3D_distance from the center of base_link
      for(unsigned int j = 0; j<cloud.channels.size(); j++)
      {
        if(cloud.channels[j].name == "3D_distance")
        {
          cloud.channels[j].values[i] = dis_3d;
        } else if(cloud.channels[j].name == "visual_bumper")
        {
          if (  (x>=bumper_front_limit_)    || 
                (x<=-1*bumper_back_limit_)  ||
                (y>=bumper_left_limit_)     ||
                (y<=-1*bumper_right_limit_)) {
            // Everything lidar detected is in free space
            cloud.channels[j].values[i] = dis_3d;
          } else {

            // remove corner
            float dis_2d = sqrt(pow(x,2)+pow(y,2)); // object 2d dist to center

            // if inside rec box also inside the circle, trigger
            if (dis_2d <= bumper_corner_remove_) {
              // bumper triggered!
              bumper_state.data = true; // even only one point in the region.
              ROS_INFO("LIDAR Bumper Triggered(%d)! cloud[%d]_dis = %4.2f.", 
                (int) bumper_state.data, i, dis_3d);

              cloud.channels[j].values[i] = 9999;
            } else {
              //if inside rec box, but outside the circle. Is corner!
              cloud.channels[j].values[i] = dis_3d;
            } // end if (rad)
          }
          break;
        }
      }
    }


    bumper_pub_.publish(bumper_state);
    scan_pub_.publish(cloud);

  }
};

int main(int argc, char** argv)
{
  
  ros::init(argc, argv, "scan_to_cloud");
  ros::NodeHandle n;
  LaserScanToPointCloud lstopc(n);
  
  ros::spin();
  
  return 0;
}